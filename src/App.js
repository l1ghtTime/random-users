import React from 'react';
import '../src/App.sass'
import Home from './Components/Home/Home';

function App() {
  return (
    <div className="App">
      <div className="container-fluid">
        <Home />
      </div>   
    </div>
  );
}

export default App;
