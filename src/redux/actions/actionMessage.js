import {MESSAGE_STATUS, MESSAGE_TYPE, } from '../actions/actionType';

export function addMessageStatus() {
    return {
        type: MESSAGE_STATUS
    }
}

export function addMessageType(payload) {
    return {
        type: MESSAGE_TYPE,
        payload: payload
    }
}