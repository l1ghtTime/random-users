import {UPDATE_STATUS, } from '../actions/actionType';

export function addUpdateStatus(value) {
    return {
        type: UPDATE_STATUS,
        payload: value,
    }
}
