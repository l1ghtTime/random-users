import {SELECTED_CARD_COUNTRY_ARRAY, } from './actionType';

export function addSelectedCardCountryArray(value) {
    return {
        type: SELECTED_CARD_COUNTRY_ARRAY,
        payload: value,
    }
}