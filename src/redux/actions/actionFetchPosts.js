import {FETCH_ALL_POSTS, SERCH_INPUT_VALUE, IS_COUNTRY, FILTER_BY_COUNTRY, } from './actionType';

export function addFetchPosts(value) {
    return {
        type: FETCH_ALL_POSTS,
        payload: value,
    }
}

export function addSerchInputValue(value) {
    return {
        type: SERCH_INPUT_VALUE,
        payload: value,
    }
}

export function addIsCountry(value) {
    return {
        type: IS_COUNTRY,
        payload: value,
    }
}



export function addFilterCounry() {
    return {
        type: FILTER_BY_COUNTRY,
    }
}