import { combineReducers } from 'redux';
import reducerFetchPosts from './reducers/reducerFetchPosts';
import reducerUsers from './reducers/reducerUsers';
import reducerUpdateStatus from './reducers/reducerUpdate';
import reducerMessage from './reducers/reducerMessage';

export default combineReducers({
    reducerFetchPosts, reducerUsers, reducerUpdateStatus, reducerMessage, 
});