import { MESSAGE_STATUS, MESSAGE_TYPE } from "../actions/actionType";


const initialState = {
    MessageStatus: false,
    MessageType: '',
}

const reducerMessage = (state = initialState, action) => {

    switch (action.type) {

        case MESSAGE_STATUS:
            return {
                ...state,
                MessageStatus: !state.MessageStatus
            }

        case MESSAGE_TYPE:
            return {
                ...state,
                MessageType: action.payload
            }

        default:
            return state;
    }

};

export default reducerMessage;