import {SELECTED_CARD_COUNTRY_ARRAY, } from "../actions/actionType";


const initialState = {
    allSelectedCountryCard: [],
}

const reducerUsers = (state = initialState, action) => {

    switch (action.type) {

        case SELECTED_CARD_COUNTRY_ARRAY:
            return {
                ...state,
                allSelectedCountryCard: state.allSelectedCountryCard.concat(action.payload)
            }

        default:
            return state;
    }

};

export default reducerUsers;