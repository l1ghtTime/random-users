import { UPDATE_STATUS, } from "../actions/actionType";


const initialState = {
    updateStatus: false,
}

const reducerUpdateStatus = (state = initialState, action) => {

    switch (action.type) {

        case UPDATE_STATUS:
            return {
                ...state,
                updateStatus: action.payload
            }

        default:
            return state;
    }

};

export default reducerUpdateStatus;