import {FETCH_ALL_POSTS, SERCH_INPUT_VALUE, IS_COUNTRY, FILTER_BY_COUNTRY, } from "../actions/actionType";


const initialState = {
    isAllPosts: [],
    serchInput: '',
    isCountry: '',
    counryCard: null,
    appliedFilters: [],
}

const reducerFetchPosts = (state = initialState, action) => {

    switch (action.type) {
        case FETCH_ALL_POSTS:
            return {
                ...state,
                isAllPosts: state.isAllPosts.concat(action.payload)
            }


        case SERCH_INPUT_VALUE:
            return {
                ...state,
                serchInput: action.payload
        }

        case IS_COUNTRY:
            return {
                ...state,
                isCountry: action.payload
        }


        case FILTER_BY_COUNTRY:

            let value = state.serchInput;
            let country = state.isCountry;
            let filteredValues = state.isAllPosts.filter(post => {
                return post.name.first.includes(value) || 
                       post.location.country.includes(country)
            });

            return {
                ...state,
                isAllPosts: filteredValues
            };





        default:
            return state;
    }

};

export default reducerFetchPosts;