import React from 'react';
import '../MessageExplanation/Message.sass';

const MessageExplanation = (props) => {
    return (
        <div className="message">
            {props.message}
        </div>
    )
}

export default MessageExplanation;
