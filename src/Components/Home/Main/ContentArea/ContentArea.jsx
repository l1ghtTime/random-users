import React from 'react';
import { Route, Switch } from 'react-router-dom';
import ContactsContent from './ContactsContent/ContactsContent';
import HomeContent from './HomeContent/HomeContent';

const ContentArea = () => {
    return (
        <div className="row">
            <Switch>
                <Route exact path="/" render={() => <HomeContent />}/>
                <Route path="/contacts" render={() => <ContactsContent />}/>
            </Switch>
        </div>
    )
}

export default ContentArea;
