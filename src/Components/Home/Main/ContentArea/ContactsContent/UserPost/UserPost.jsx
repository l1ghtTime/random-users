import React from 'react';
import {useSelector } from 'react-redux';

const UserPost = ({ posts }) => {
    const postsAll = useSelector(state => state.reducerFetchPosts.isAllPosts);
    
    console.log("ALL POST", postsAll);

    return (
        <nav className="contacts__users-menu users-menu">
            <ul className="users-menu__list">
                {/* ИСПОЛЬЗОВАЛ index вместо id потому что в некоторых карточках в API id равен null */}


                {
                   posts.length && posts.map((post, index) => {
                        return <li key={index} className="users-menu__item">
                            <span>{post.gender}</span>
                            <span>{post.location.country}</span>
                            <span>{post.name.first}</span>
                        </li>
                    })
                }

            </ul>
        </nav>
    )
}

export default UserPost;
