import React from 'react';
import { Link } from 'react-router-dom';

const Pagination = ({ postsPerPege, totalPosts, paginate }) => {

    let totalPost = Math.ceil(totalPosts / 10) * 10;

    const pageNumbers = [];

    for (let i = 1; i <= (totalPost / postsPerPege); i++) {
        pageNumbers.push(i);
    }

    return (
        <nav>
            <ul className="pagination">
                {pageNumbers.map(number => (
                    <li key={number} className="page-item pagination__item">
                        <Link to="/contacts"className="page-link pagination__link" onClick={() => paginate(number)}>
                            {number}
                        </Link>
                    </li>
                ))}
            </ul>
        </nav>
    )
}

export default Pagination;
