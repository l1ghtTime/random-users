import React from 'react';
import { useDispatch } from 'react-redux';
import {addFilterCounry, addSerchInputValue } from '../../../../../../../redux/actions/actionFetchPosts';

const Search = () => {

    const dispatch = useDispatch();
    
    function handleChange(event) {
        dispatch(addSerchInputValue(event.target.value));
        dispatch(addFilterCounry())
    }

    return (
        <div className="col-12 mb-4 col-sm-12 mb-sm-4 col-md-5 mb-md-0 col-lg-5 mb-lg-0 col-xl-6 mb-xl-0">
            <input type="text" className="navigation-line__search" placeholder="Search by full name" onChange={handleChange}/>
        </div>
    )
}

export default Search;
