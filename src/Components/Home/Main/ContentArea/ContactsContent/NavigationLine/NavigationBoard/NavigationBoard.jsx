import React from 'react';
import { Link } from 'react-router-dom';
import updateDate from '../../../../../../../image/update.svg';
import tiled from '../../../../../../../image/tiled.svg';
import tabular from '../../../../../../../image/tabular.svg';
import { useDispatch, useSelector } from 'react-redux';
import { addUpdateStatus } from '../../../../../../../redux/actions/actionUpdate';
import MessageExplanation from '../../../../../../MessageExplanation/MessageExplanation';
import { addMessageStatus, addMessageType } from '../../../../../../../redux/actions/actionMessage';

const NavigationBoard = () => {

    const dispatch = useDispatch();
    const message = useSelector(state => state.reducerMessage);
    const messageType = useSelector(state => state.reducerMessage.MessageType);

    function handleUpdate(event) {
        dispatch(addUpdateStatus(true))

        setTimeout(() => {
            dispatch(addUpdateStatus(false))
        }, 1000)
    }

    function handleHover(event) {
        dispatch(addMessageStatus());
        dispatch(addMessageType(event.target.getAttribute("datastatus")))
    }

    function handleHoverLeave(event) {
        dispatch(addMessageStatus());
    }

    return (
        <div className="col-12 mb-0 col-sm-4 mb-sm-4 col-md-2 mb-md-0 col-lg-2 mb-lg-0 col-xl-2 mb-xl-0">
            <div className="navigation-line__board navigation-board">

                <Link to="/contacts" className="navigation-board__link navigation-board__link--update" datastatus="update" onClick={handleUpdate} onMouseEnter={handleHover} onMouseLeave={handleHoverLeave}>
                    <img src={updateDate} className="navigation-board__img" alt="update-icon" />
                    { message.MessageStatus && messageType === 'update' ? <MessageExplanation message="Update data"/> : null }
                </Link>

                <Link to="/contacts" className="navigation-board__link navigation-board__link--view" datastatus="tiled" onMouseEnter={handleHover} onMouseLeave={handleHoverLeave}>
                    <img src={tiled} className="navigation-board__img" alt="tiled-icon" />
                    { message.MessageStatus && messageType === 'tiled' ? <MessageExplanation message="Tiled view"/> : null }
                </Link>

                <Link to="/contacts" className="navigation-board__link navigation-board__link--view" datastatus="tabular" onMouseEnter={handleHover} onMouseLeave={handleHoverLeave}>
                    <img src={tabular} className="navigation-board__img" alt="tabular-icon" />
                    { message.MessageStatus && messageType === 'tabular' ? <MessageExplanation message="Tabular view"/> : null }
                </Link>

            </div>
        </div>
    )
}

export default NavigationBoard;
