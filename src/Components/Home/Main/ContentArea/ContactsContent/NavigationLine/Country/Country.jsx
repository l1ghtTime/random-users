import React from 'react';
import { useDispatch } from 'react-redux';
import { addIsCountry } from '../../../../../../../redux/actions/actionFetchPosts';

const Country = ({posts}) => {

    const dispatch = useDispatch();

    function handleChange(event) {
        dispatch(addIsCountry(event.target.value))
    }

    return (
        <div className="col-12 mb-4 col-sm-4 mb-sm-4 col-md-3 mb-md-0 col-lg-3 mb-lg-0 col-xl-2 mb-xl-0">
        <select className="navigation-line__select" onChange={handleChange}>
            <option>Chose a country</option> 
            {/* ИСПОЛЬЗОВАЛ index вместо id потому что в некоторых карточках в API id равен null */}
            {
                posts.map((post, index) => {
                    return <option key={index}>{post.location.country}</option> 
                })
            }
        </select>
    </div>

    )
}

export default Country;
