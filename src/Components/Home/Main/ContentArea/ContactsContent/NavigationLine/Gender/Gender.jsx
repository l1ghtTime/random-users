import React from 'react';

const Gender = () => {
    return (
        <div className="col-12 mb-4 col-sm-4 mb-sm-4 col-md-2 mb-md-0 col-lg-2 mb-lg-0 col-xl-2 mb-xl-0">
            <select className="navigation-line__select">
                <option>Male</option>
                <option>Female</option>
            </select>
        </div>
    )
}

export default Gender;
