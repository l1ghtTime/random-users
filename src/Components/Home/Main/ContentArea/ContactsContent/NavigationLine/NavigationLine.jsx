import React from 'react';
import Search from './Search/Search';
import Gender from './Gender/Gender';
import Country from './Country/Country';
import NavigationBoard from './NavigationBoard/NavigationBoard';

const NavigationLine = ({posts}) => {
    return (
        <div className="row">
            <div className="contacts__navigation-line navigation-line">
                <Search />
                <Gender />
                <Country posts={posts}/>
                <NavigationBoard />
            </div>
        </div>
    )
}

export default NavigationLine;
