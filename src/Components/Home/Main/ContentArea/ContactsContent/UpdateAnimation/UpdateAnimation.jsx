import React from 'react';

const UpdateAnimation = () => {
    return (
        <div className="contacts__update-cover update-cover">
            <div className="spinner-border text-primary update-cover__spiner" role="status">
                <span className="sr-only">Loading...</span>
            </div>
        </div>
    )
}

export default UpdateAnimation;
