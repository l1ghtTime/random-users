import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { addFetchPosts } from '../../../../../redux/actions/actionFetchPosts';
import random from '../../../../../shortDecision/random';
import NavigationLine from './NavigationLine/NavigationLine';
import Pagination from './Pagination/Pagination';
import UpdateAnimation from './UpdateAnimation/UpdateAnimation';
import UserPost from './UserPost/UserPost';


const ContactsContent = () => {

    // const [posts, setPosts] = useState([]);
    const posts = useSelector(state => state.reducerFetchPosts.isAllPosts);
    const [currentPage, setCurrentPage] = useState(1);
    const [postsPerPage] = useState(10);
    const dispatch = useDispatch();

    useEffect(() => {
        fetch(`https://randomuser.me/api/?results=${random()}`)
            .then(response => response.json())
            .then(data => dispatch(addFetchPosts(data.results)))
    }, [dispatch]);

    const indexOfLastPost = currentPage * postsPerPage;
    const indexOfFirstPost = indexOfLastPost - postsPerPage;
    const currentPosts = posts.slice(indexOfFirstPost, indexOfLastPost);




    const paginate = (pageNumber) => setCurrentPage(pageNumber);
    const update = useSelector(state => state.reducerUpdateStatus)

    return (
        <div className="contacts">
            <NavigationLine posts={currentPosts} />
            <UserPost posts={currentPosts} />
            <Pagination postsPerPege={postsPerPage} totalPosts={posts.length} paginate={paginate} />
            {update.updateStatus ? 
                <UpdateAnimation />
                : null
            }
        </div>
    )
}

export default ContactsContent;
