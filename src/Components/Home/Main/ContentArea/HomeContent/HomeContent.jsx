import React from 'react';
import reactLogo from '../../../../../image/react-logo.svg';

const HomeContent = () => {
    return (
        <div className="col-lg-12">
            <div className="home-content">
                <img src={reactLogo} className="home-content__logo" alt="React Logo"/>
            </div>
        </div>
    )
}

export default HomeContent;
