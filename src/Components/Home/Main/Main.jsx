import React from 'react';
import backDrop from '../../../image/backdrop.jpg';
import ContentArea from './ContentArea/ContentArea';

const Main = () => {
    return (
        <main className="main" style={{background: `url(${backDrop})`}}>
            <ContentArea />
        </main>
    )
}

export default Main;
