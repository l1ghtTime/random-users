import React from 'react';
import { Link } from 'react-router-dom';

const HeaderNavigation = () => {
    return (
        <nav className="navigation">
            <ul className="navigation__list">
                <li className="navigation__item">
                    <Link to="/" className="navigation__link">Home</Link>
                </li>

                <li className="navigation__item">
                    <Link to="/contacts" className="navigation__link">Contacts</Link>
                </li>
            </ul>
            
        </nav>
    )
}

export default HeaderNavigation;
