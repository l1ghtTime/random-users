import React from 'react';
import { Link } from 'react-router-dom';
import logo from '../../../../image/wezom-logo.svg';


const Logo = () => {
    return (
        <div className="header__box-logo">
            <Link to="/" className="logo header__logo">
                <img src={logo} alt="logo"/>
            </Link>
        </div>
    )
}

export default Logo;
