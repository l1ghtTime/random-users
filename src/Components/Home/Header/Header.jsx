import React from 'react';
import HeaderNavigation from './HeaderNavigation/HeaderNavigation';
import HeaderSignIn from './HeaderSignIn/HeaderSignIn';
import Logo from './Logo/Logo';

const Header = () => {
    return (
        <header className="header">
            <div className="row">
                <div className="col-3 col-sm-2 col-md-1 col-md-1 col-lg-1 col-xl-1">
                    <Logo />
                </div>

                <div className="col-6 offset-3 col-sm-1 offset-sm-0 col-md-1 offset-md-0 col-lg-1 offset-lg-0 col-xl-1 offset-xl-0">
                    <HeaderNavigation />
                </div>

                <div className="col-5 offset-7 col-sm-3 offset-sm-6 col-md-2 offset-md-8 col-lg-2 offset-lg-8 col-xl-2 offset-xl-8 d-flex justify-content-center">
                    <HeaderSignIn />
                </div>        
            </div>
        </header>
    )
}

export default Header;
