import React from 'react';
import '../Home/Home.sass';
import Footer from './Footer/Footer';
import Header from './Header/Header';
import Main from './Main/Main';

const Home = () => {
    return (
        <React.Fragment>
            <Header />
            <Main />
            <Footer />
        </React.Fragment>
    )
}

export default Home;
