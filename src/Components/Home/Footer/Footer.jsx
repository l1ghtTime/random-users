import React from 'react';

const Footer = () => {
    return (
        <footer className="footer">
            2020 © Wezom React-Redux Test
        </footer>
    )
}

export default Footer;
